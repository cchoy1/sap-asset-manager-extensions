import { BaseObservable } from '../../../observables/BaseObservable';
import { Context } from '../../../context/Context';
import { IControl } from '../../../controls/IControl';
import { BaseExtensionObservable } from './BaseExtensionObservable';

export class BarcodeScannerObservable extends BaseExtensionObservable {
    public constructor(control: IControl) {
        super(control);
    }   
};
