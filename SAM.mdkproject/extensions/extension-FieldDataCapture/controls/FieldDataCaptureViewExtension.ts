import { BaseControl } from '../../../controls/BaseControl';
import { FormCellContainerView } from 'mdk-sap';
import { BaseFormCell } from '../../../controls/formCell/BaseFormCell';
import { ControlFactorySync } from '../../../controls/ControlFactorySync';
import { EventHandler } from '../../../EventHandler';
import { I18nHelper } from '../../../utils/I18nHelper';
import { PropertyTypeChecker } from '../../../utils/PropertyTypeChecker';
import { ClientSettings } from '../../../storage/ClientSettings';
import { FieldDataCaptureDefinition } from './FieldDataCaptureDefinition';
import { asService } from '../../../data/EvaluateTarget';
import { TargetPathInterpreter } from '../../../targetpath/TargetPathInterpreter';
import { IDataService } from '../../../data/IDataService';
import { IFormCellProxy } from '../../../context/IClientAPI';
import { ListPickerFormCellProxy, FormCellControlProxy} from '../../../context/ClientAPI';
import { MDKPage } from '../../../pages/MDKPage';
import { IDefinitionProvider } from '../../../definitions/IDefinitionProvider';
import { FieldDataCaptureChangeSetActionRunner, IFieldDataCaptureChangeSetActionRunnerDelegate }
  from './FieldDataCaptureChangeSetActionRunner';
import { IActionFactory } from '../../../actions/IActionFactory';
import { IAction } from '../../../actions/IAction';
import { Context } from '../../../context/Context';
import * as app from 'tns-core-modules/application';

export class FieldDataCaptureViewExtension
  extends BaseControl
  implements IFieldDataCaptureChangeSetActionRunnerDelegate {

  private _cells: BaseFormCell[] = [];
  private nuiStyleClass: string;
  private fieldDataCaptureDefinition: FieldDataCaptureDefinition;

  private sectionsContexts = [];
  private parentPage: MDKPage;
  private bindingStash: any[];
  private activeCells: BaseFormCell[] = [];
  private firstSectionIndex: number;
  private lastSectionIndex: number;
  private changeSetGroupIndex: number;
  private addedCells: number = 0;
  private isInPopover: boolean;

  private allControlsAreBuilt: Promise<any>;

  public initialize(props) {
    super.initialize(props);

    this.parentPage = this.page() as MDKPage;
    // As this control mimics the FormCellContainer, we reuse its definition
    let fieldDataCaptureDefinitionData = props.definition.data.ExtensionProperties;
    this.fieldDataCaptureDefinition = new FieldDataCaptureDefinition(
      '',
      fieldDataCaptureDefinitionData,
      props.definition.data.ExtensionProperties.parent);
  }

  public bind(): Promise<any> {
    this.bindingStash =  this.parentPage.context.binding;

    return this.readEntities().then(readResults => {
      return this.configureModel(readResults).then(() => {
        return this.createUI().then(() => {
          let promiseRunner = (sections, index): Promise<any> => {
            if (index >= sections.length) {
              return Promise.resolve();
            }
            if (sections[index].OnLoaded) {
              return this.executeActionOnGroup(sections[index].OnLoaded, index).then(() => {
                return promiseRunner(sections, index + 1);
              });
            }
            return promiseRunner(sections, index + 1);
          };
          return promiseRunner(this.fieldDataCaptureDefinition.sections, 0).then(() => {
            this.redraw();
          });
        });
      });
    });
  }

  public executeChangeSet(changeSetPath: string, groupIndex: number = -1) {
    this.changeSetGroupIndex = groupIndex;
    let changeSetRunner = new FieldDataCaptureChangeSetActionRunner(this);
    let definition = IDefinitionProvider.instance().getDefinition(changeSetPath);
    let changeSet: IAction = IActionFactory.Create(definition);

    return changeSetRunner.run(changeSet).then(result => {
      this.cleanup(this.getChangeSetBeginIndex() + this.getChangeSetEntityCount() - 1);
    }).catch(error => {
      // TODO: check if actions need to be taken here
      let test  = 'test';
    });
  }

  public executeActionOnEverySection(actionPath: string) {
    this.firstSectionIndex = 0;
    this.lastSectionIndex = this.sectionsContexts.length - 1;
    this.executeRecursive(actionPath, 0);
  }

  public executeActionOnGroup(actionPath: string, groupIndex: number): Promise<any> {
    let skippedSections = this.fieldDataCaptureDefinition.entityCounts
      .slice(0, groupIndex)
      .reduce((previous, current) => previous + current, 0);
    let entityCount = this.fieldDataCaptureDefinition.entityCounts[groupIndex];
    this.firstSectionIndex = skippedSections;
    this.lastSectionIndex = skippedSections + entityCount - 1;
    return this.executeRecursive(actionPath, skippedSections);
  }

  public willExecuteAction(sectionIndex: number) {
    this.performPreActionSetup(sectionIndex);
  }

  public didExecuteAction(sectionIndex: number) {
    this.cleanup(sectionIndex);
  }

  public getChangeSetEntityCount(): number {
    let actionsCount = this.sectionsContexts.length;
    if (this.changeSetGroupIndex >= 0) {
      actionsCount = this.fieldDataCaptureDefinition.entityCounts[this.changeSetGroupIndex];
    }
    return actionsCount;
  }

  public getChangeSetBeginIndex(): number {
    let beginIndex = 0;
    if (this.changeSetGroupIndex >= 0) {
      beginIndex = this.fieldDataCaptureDefinition.entityCounts
        .slice(0, this.changeSetGroupIndex)
        .reduce((previous, current) => previous + current, 0);
    }
    return beginIndex;
  }

  public getCellProxyWithName(name: string): IFormCellProxy {
    let foundCell = this.activeCells.find((cell) => cell.definition().getName() === name);
    if (!foundCell) {
      foundCell = this._cells.find((cell) => cell.definition().getName() === name);
    }

    if (foundCell && foundCell.definition().getType() === 'Control.Type.FormCell.ListPicker') {
      return new ListPickerFormCellProxy(foundCell.context);
    }
    return new FormCellControlProxy(foundCell.context);
  }

  public viewIsNative() {
    return true;
  }

  public setStyle(style: string) {
    this.nuiStyleClass = style;
  }

  public redraw(builtData: any = undefined) {
    if (builtData) {
      // We just built and are being given the latest builtData, so just use it
      this.view().updateCells(builtData, this.nuiStyleClass);
    } else {
      // No data? That means we are coming here from a rule where redraw() was
      // called on the API or some other control setter that resulted on that,
      // so we need to make sure we use newly built data for all properties
      //
      // Also, in this situation specially when redraw() is called in the OnLoaded
      // handler of the page, this could be called before the initial build
      // of all controls. This will cause a crash on the native side because we
      // would be calling to updateCells() before _buildControls() is finished and
      // hence before populate() is done in which case some cells will not exist yet.
      return this.allControlsAreBuilt.then(() => {
        return Promise.all(this.cells.map((control: BaseFormCell) => {
          return control.updateFormCellModel();
        })).then((newBuiltData) => {
          this.view().updateCells(newBuiltData, this.nuiStyleClass);
        });
      });
    }
  }

  public get cells() {
    return this._cells;
  }

  public get controls() {
    return this._cells;
  }

  public bindValue(value: any): Promise<any> {
    // Nothing to bind...move on.
    return Promise.resolve();
  }

  public updateCell(control: BaseFormCell) {
    let indexPath = this.fieldDataCaptureDefinition.indexPath(control.definition().getName());
    if (this._isValidIndexPath(indexPath)) {
      if (app.android) {
        control.updateFormCellModel();
      } else {
        control.build().then(data => {
          // For Android...and eventually iOS, we update the native form cell directly instead of through
          // the container
          this.view().updateCell(data, indexPath.row, indexPath.section);
        });
      }
    }
  }

  // Workaround to BCP 1880677511
  // update cell by input data without fetching entire dataset from obeservable for the cell
  public updateCellByProperties(control: BaseFormCell, data: any) {
    let indexPath = this.definition().indexPath(control.definition().getName());
    if (this._isValidIndexPath(indexPath)) {
      if (app.ios) {
        this.view().updateCell(data, indexPath.row, indexPath.section);
      }
    }
  }

  public onLoaded() {
    // Run formcell controls onLoaded
    let formcellOnLoadedPromises = [];
    for (let formcell of this.cells) {
        let promise = formcell.onLoaded();
        formcellOnLoadedPromises.push(promise);
    }
    Promise.all(formcellOnLoadedPromises).then((onLoadedResults) => {
      let redrawNeeded = false;
      for (let onLoadedResult of onLoadedResults) {
        if (onLoadedResult) {
          redrawNeeded = true;
          break;
        }
      }
      if (redrawNeeded) {
        this.redraw();
      }
    });

    // Run definition OnLoaded event
    this.page().runOnLoadedEvent();
  }

  public getContext(sectionIndex: number) {
    let binding = this.sectionsContexts[sectionIndex].binding;
    binding.pageBinding = this.bindingStash;
    return new Context(binding, this.page());
  }

  // TODO: This is temporary, to be removed once the changeSets get implemented
  private executeRecursive(actionPath: string, sectionIndex: number): Promise<any> {
    this.performPreActionSetup(sectionIndex);

    let context = this.getContext(sectionIndex);

    let eventHandler = new EventHandler();
    return eventHandler.executeActionOrRule(actionPath, context).then(value => {
      this.cleanup(sectionIndex);

      if (sectionIndex < this.lastSectionIndex) {
        return this.executeRecursive(actionPath, ++sectionIndex);
      }
      return Promise.resolve();
    }).catch(error => {
      return this.cleanup(sectionIndex);
    });
  }

  private readEntities(): Promise<any> {
    let sectionReadPromises = this.fieldDataCaptureDefinition.sections.map((section) => {
      if (section.Target) {
        if (PropertyTypeChecker.isTargetPath(section.Target)) {
          let interpreter = new TargetPathInterpreter(this.context);
          const data = interpreter.evaluateTargetPathForValue(section.Target);
          return Promise.resolve(data);
        } else {
          return asService(section, this.context).then(service => {
            return IDataService.instance().read(service);
          });
        }
      } else {
        return Promise.resolve([this.context.binding]);
      }
    });
    return Promise.all(sectionReadPromises);
  }

  private configureModel(readResults: any): Promise<any> {
    let entityCounts = readResults.map((entities) => {return entities.length; });
    this.fieldDataCaptureDefinition.entityCounts = entityCounts;

    for (let readResult of readResults) {
      let sectionContexts = readResult.map((entity) => {return {binding: entity}; });
      this.sectionsContexts.push(...sectionContexts);
    }
    return Promise.resolve();
  }

  private createUI(): Promise<any> {
    const bridge = new FormCellContainerView(this.page(), this, this.formcellData);
    this.setView(bridge);

    this._createControls();
    this.allControlsAreBuilt = this._buildControls();
    return this.allControlsAreBuilt;
  }

  private _createControls() {
    let promises = [];
    let definitionIndex = 0;
    this.cells.length = 0;

    this.fieldDataCaptureDefinition.numberOfRowsInSection.forEach((rowCount: number, index: number) => {
      let cellContext = this.sectionsContexts[index];
      for (let j = 0; j < rowCount; j++) {
        let cellDefinition = this.fieldDataCaptureDefinition.getControlDefs()[definitionIndex];
        const cell: any = ControlFactorySync.Create(this.page(), cellContext, null, cellDefinition);
        this.cells.push(cell);
        cell.parent = this;
        definitionIndex++;
      }
    });
  }

  private _buildControls(): Promise<any> {
    return Promise.all(this.cells.map(control => {
      control.parent = this;
      return control.build().then(() => {
        return control.bind();
      });
    })).then(() => {
      // Create native views after all of the form cells are bound.  Since the promises in the control
      // binding above can resolve out of order, we can end up with form cells created out of order.
      return Promise.all(this.cells.map(control => {
        return control.createFormCellModel(control.builder.builtData).then((mdkFormCell) => {
          this.view().addFormCell(mdkFormCell);
          return control.builder.builtData;
        });
      }));
    });
  }

  private performPreActionSetup(sectionIndex: number) {
    this.activeCells = this.getCellsForSection(sectionIndex);
    this.replacePageBindingWithBindingFromSection(sectionIndex);
    this.updateCellsNamesForSection(sectionIndex, false);
    this.setCellsFromSectionAsChildControlsOfPage(sectionIndex);
  }

  private cleanup(sectionIndex: number) {
    this.updateCellsNamesForSection(sectionIndex, true);
    this.parentPage.context.binding = this.bindingStash;
    this.removeCellsFromPage();
  }

  private replacePageBindingWithBindingFromSection(sectionIndex: number) {
    this.parentPage.context.binding = this.sectionsContexts[sectionIndex].binding;
  }

  private setCellsFromSectionAsChildControlsOfPage(sectionIndex: number) {
    for (let cell of this.activeCells) {
      this.parentPage.addChildControl(cell);
      this.addedCells++;
    }
  }

  private updateCellsNamesForSection(sectionIndex: number, isNeedSuffix: boolean) {
    let cellIndexBegin = this.fieldDataCaptureDefinition.numberOfRowsInSection
      .slice(0, sectionIndex)
      .reduce((previous, current) => previous + current, 0);

    return this.getCellsForSection(sectionIndex).map((cell, index) => {
      let cellName: string;

      if (isNeedSuffix) {
        cellName = this.fieldDataCaptureDefinition.getControlNamesWithSuffixes()[cellIndexBegin + index];
      } else {
        cellName = this.fieldDataCaptureDefinition.getControlNamesWithoutSuffixes()[cellIndexBegin + index];
      }

      cell.definition().data._Name = cellName;
      return cell;
    });
  }

  private getCellsForSection(sectionIndex: number): BaseFormCell[] {
    let cellIndexBegin = this.fieldDataCaptureDefinition.numberOfRowsInSection
      .slice(0, sectionIndex)
      .reduce((previous, current) => previous + current, 0);

    let nbRowsInSection = this.fieldDataCaptureDefinition.numberOfRowsInSection[sectionIndex];
    let cellIndexEnd = cellIndexBegin + nbRowsInSection;
    return this.cells.slice(cellIndexBegin, cellIndexEnd);
  }

  private isFirstSection(sectionIndex: number): Boolean {
    return sectionIndex === this.firstSectionIndex;
  }

  private isLastSection(sectionIndex: number): Boolean {
    return sectionIndex === this.lastSectionIndex;
  }

  private removeCellsFromPage() {
    for (let i = 0; i < this.addedCells; i++) {
      this.parentPage.controls.pop();
    }
    this.addedCells = 0;
  }

  private _isValidIndexPath(indexPath: {row, section}): boolean {
    return indexPath.row !== -1 &&  indexPath.section !== -1;
  }

  /**
   * This method is to localize section names
   *
   * @param sectionNames string array of section names to be localized
   * @return {string[]} localized section names
   *
   */
  private _localizeSectionNames(sectionNames: string[]): string[] {
    let localizedSectionNames: string[] = [];
    sectionNames.map((name) => {
      if (PropertyTypeChecker.isLocalizableString(name)) {
        localizedSectionNames.push(I18nHelper.parseLocalizableString(name, this.context));
      } else {
        localizedSectionNames.push(name);
      }
    });

    return localizedSectionNames;
  }

  private get formcellData(): any {
    let isInPopover = this._props.page.isPopover ? this._props.page.isPopover : false;
    let localizedSectionNames = this._localizeSectionNames(this.fieldDataCaptureDefinition.sectionNames);

    const formcellData = {
      isInPopover,
      locale: ClientSettings.getAppLocale(),
      numberOfRowsInSection: this.fieldDataCaptureDefinition.numberOfRowsInSection,
      numberOfSections: this.fieldDataCaptureDefinition.sectionCount,
      sectionNames: localizedSectionNames,
    };

    return formcellData;
  }
}
