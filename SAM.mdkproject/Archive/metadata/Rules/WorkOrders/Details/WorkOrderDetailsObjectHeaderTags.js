import libWoMobile from '../../MobileStatus/MobileStatusLibrary';
import libClock from '../../ClockInClockOut/ClockInClockOutLibrary';

export default function WorkOrderDetailsObjectHeaderTags(context) {
    let binding = context.getBindingObject();
    var tags = [context.getBindingObject().OrderType, undefined, undefined];
    if (binding.MarkedJob && binding.MarkedJob.PreferenceValue && binding.MarkedJob.PreferenceValue === 'true') {
        tags[2] = context.localizeText('FAVORITE');
    }
    let status = libWoMobile.getMobileStatus(context.binding);
    if (libClock.isBusinessObjectClockedIn(context)) {
        tags[1] = context.localizeText(status) + '-' + context.localizeText('clocked_in');
    } else if (status) {
        tags[1] = context.localizeText(status);
    }
    return tags;
}
