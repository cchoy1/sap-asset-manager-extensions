import PRTMeasuringPointsCount from '../PRTTotalCount';
import enableMeasurementCreate from '../../../UserAuthorizations/Measurements/EnableMeasurementCreate';

export default function PRTMeasuringPointTakeReadingsNavBtnVisible(context) {
  context.getPageProxy = () => context;
  return PRTMeasuringPointsCount(context).then(result => {
    return result > 0 ? enableMeasurementCreate(context) : false;
  });
}
