/*
 COPYRIGHT 2017 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

#import <ArcGIS/AGSLoadableBase.h>

@class AGSMap;
@class AGSOfflineMapSyncJob;
@class AGSOfflineMapSyncParameters;
@class AGSOfflineMapUpdateCapabilities;
@class AGSOfflineMapUpdatesInfo;

/** @file AGSOfflineMapSyncTask.h */ //Required for Globals API doc

/** @brief A task to sync an offline map
 
 Instances of this class represent a task that can be used to synchronize changes between feature layers and tables of an offline map and their originating ArcGIS Feature services.
 
 @see `AGSOfflineMapTask` to take a map offline
 @since 100.1
 */
@interface AGSOfflineMapSyncTask : AGSLoadableBase

NS_ASSUME_NONNULL_BEGIN

#pragma mark -
#pragma mark initializers

- (instancetype)init NS_UNAVAILABLE;

/** Initialize this task with the provided offline map.
 @param map Offline map whose feature layers and tables need to be synced
 @return initialized task
 @since 100.1
 */
-(instancetype)initWithMap:(AGSMap*)map;

/** Initialize this task with the provided offline map.
 @param map Offline map whose feature layers and tables need to be synced
 @return initialized task
 @since 100.1
 */
+(instancetype)offlineMapSyncTaskWithMap:(AGSMap*)map;

#pragma mark -
#pragma mark properties

/** Offline map whose feature layers and tables need to be synced with their originating service.
 @since 100.1
 */
@property (nonatomic, strong, readonly) AGSMap *map;

/** Describes the methods used for obtaining updates to the offline map that was used to create this task.
 You can use this property to determine whether an offline map is configured to use the preplanned scheduled updates
 workflow (see @c AGSOfflineMapUpdateCapabilities#supportsScheduledUpdatesForFeatures) or to sync directly with feature
 services (see OfflineMapUpdateCapabilities.supportsSyncWithFeatureServices).
 
 This property will return @c nil until the offline map sync task is loaded.
 @since 100.6
 */
@property (nullable, nonatomic, strong, readonly) AGSOfflineMapUpdateCapabilities *updateCapabilities;

#pragma mark -
#pragma mark methods

/** Asynchronously retrieves @c AGSOfflineMapUpdatesInfo for the offline map that was used to construct this task.
 The returned @c AGSOfflineMapUpdatesInfo provides high level information on what updates are available for this
 offline map. Information is provided on:
 - online changes that can be applied to update your offline map
 - local changes from your offline map that can be sent back to the online services
 
 Calling this method provides high-level information on the available updates. It can help you to determine
 whether to call @c AGSOfflineMapSyncTask#offlineMapSyncJobWithParameters: immediately, based upon factors such as current disk
 space and network availability. Examine these properties before starting the potentially time-consuming
 offline map sync process.
 
 The resulting @c AGSOfflineMapUpdatesInfo provides a snap-shot of available updates when this method was called.
 To check for new updates you need to call this method again.
 @param completion Block that is invoked when the operation finishes. The #result parameter is populated if the operation completed successfully, otherwise the #error parameter is populated.
 @return An operation which can be canceled.
 @since 100.6
 */
-(id<AGSCancelable>)checkForUpdatesWithCompletion:(void(^)(AGSOfflineMapUpdatesInfo * __nullable result, NSError * __nullable error))completion;

/** Generates @c AGSOfflineMapSyncParameters for the map used to construct this task.
 The parameters will be pre-populated with default values, appropriate for updating the
 feature data in this offline map.
 
 The default parameters will reflect the mobile geodatabases used by the offline map.
 @param completion block that is invoked when the operation finishes. The #result parameter is populated if the operation completed successfully, otherwise the #error parameter is populated.
 @return operation which can be canceled
 @since 100.6
 */
-(id<AGSCancelable>)defaultOfflineMapSyncParametersWithCompletion:(void(^)(AGSOfflineMapSyncParameters * __nullable result, NSError * __nullable error))completion;

/** Returns a job which can be used to synchronize the offline map. The result of the job will be an `AGSOfflineMapSyncResult` object.
 @note The job is dormant and needs to be explicitly started using `AGSOfflineMapSyncJob#startWithStatusHandler:completion:`
 @param parameters specifying how the map's feature layers and tables should be synced with the service
 @return job representing the progress on the server. The result of the job will be an `AGSOfflineMapSyncResult` object
 @since 100.1
 @license{Basic, when sync direction is `Bidirectional` or `Upload` with public feature services or when using a private feature service with any sync direction. "Public" feature services means services hosted on the Internet and available anonymously (not secured). "Private" includes all other scenarios such as feature services hosted on a local network within an enterprise or feature services hosted on the Internet but secured}
 @ingroup licensing
 */
-(AGSOfflineMapSyncJob *)offlineMapSyncJobWithParameters:(AGSOfflineMapSyncParameters *)parameters;

@end

NS_ASSUME_NONNULL_END
