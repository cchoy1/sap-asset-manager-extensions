import libEval from '../../Common/Library/ValidationLibrary';

export default function UpdateOnlineQueryOptions(context) {
    // Get values from controls
    let plant = '';
    try {
        plant = context.getPageProxy().evaluateTargetPath('#Control:PlantLstPkr/#SelectedValue');
    } catch (exc) {
        plant = context.getPageProxy().binding.Plant;
    }

    let slocValues = context.getPageProxy().evaluateTargetPath('#Control:StorageLocationLstPkr/#value');
    let materialNumber = context.getPageProxy().evaluateTargetPath('#Control:MaterialNumber').getValue();
    let materialDescription = context.getPageProxy().evaluateTargetPath('#Control:MaterialDescription').getValue();
    let onlineSwitchValue = context.getPageProxy().evaluateTargetPath('#Control:OnlineSwitch').getValue();

    if (context.binding['@odata.type'] === '#sap_mobile.BOMItem') {
        materialNumber = context.binding.Component;
    }

    if (libEval.evalIsEmpty(materialNumber)) {
        materialNumber = context.getPageProxy().binding.MaterialNum;
    }

    // Get target specifier
    let materialListPicker = context.getPageProxy().evaluateTargetPathForAPI('#Control:MaterialLstPkr');
    let materialLstPkrSpecifier = materialListPicker.getTargetSpecifier();
    let materialLstPkrQueryOptions = '$orderby=MaterialNum&$expand=Material,MaterialPlant';

    let newFilterOpts = [`Plant eq '${plant}'`];
    if (slocValues.length > 0) {
        let slocValuesQuery = [];
        for (let i in slocValues) {
            if (!libEval.evalIsEmpty(slocValues[i])) {
                slocValuesQuery.push(`StorageLocation eq '${slocValues[i].ReturnValue}'`);
            }
        }
        if (slocValuesQuery.length > 0) {
            newFilterOpts.push(`(${slocValuesQuery.join(' or ')})`);
        }
    }

    if (materialDescription && !(context.binding['@odata.type'] === '#sap_mobile.BOMItem')) {
        newFilterOpts.push(`substringof('${materialDescription}', MaterialDesc)`);
    }
    materialLstPkrQueryOptions += '&$filter=' + newFilterOpts.join(' and ');
    if (onlineSwitchValue) {
        materialLstPkrSpecifier.setObjectCell({
            'PreserveIconStackSpacing': false,
            'Title': '{{#Property:Material/#Property:Description}} ({{#Property:MaterialNum}})',
            'Subhead': '{{#Property:StorageLocationDesc}} - {{#Property:Plant}}',
            'Footnote' : '$(L,available_qty_x_x, {UnrestrictedQuantity}, {Material/BaseUOM})',
        });
    } else {
        materialLstPkrSpecifier.setObjectCell({
            'PreserveIconStackSpacing': false,
            'Title': '{{#Property:Material/#Property:Description}} ({{#Property:MaterialNum}})',
            'Subhead': '{{#Property:StorageLocationDesc}} - {{#Property:Plant}}',
            'Footnote' : '{{#Property:UnrestrictedQuantity} {{#Property:Material/BaseUOM}}',
        });
    }
    materialLstPkrSpecifier.setEntitySet('MaterialSLocs');
    materialLstPkrSpecifier.setReturnValue('{@odata.readLink}');
    materialLstPkrSpecifier.setQueryOptions(materialLstPkrQueryOptions);
    if (onlineSwitchValue) {
        materialLstPkrSpecifier.setService('/SAPAssetManager/Services/OnlineAssetManager.service');
    } else {
        materialLstPkrSpecifier.setService('/SAPAssetManager/Services/AssetManager.service');
    }
    return materialListPicker.setTargetSpecifier(materialLstPkrSpecifier, false).then(() => {
        if (materialNumber) {
            materialListPicker.setValue(`MaterialSLocs(Plant='${context.getPageProxy().binding.Plant}',StorageLocation='${context.getPageProxy().binding.StorageLocation}',MaterialNum='${materialNumber}')`);
        }
    });
}
