declare var AnalyticsBridge: any;

import {ControlDelegate} from '../Common/ControlDelegate';

export class Analytics {
    // Returns a view controller
    public create(params, dataService, analyticsExtension): any {
        let bridge = AnalyticsBridge.new();
        analyticsExtension._bridge = bridge;
        let analyticsDelegate = ControlDelegate.initWithDataServiceAndBridge(dataService, bridge, analyticsExtension);
        return bridge.createWithParamsAndDelegate(params, analyticsDelegate);
    }
};
