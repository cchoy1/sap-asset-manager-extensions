/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

/** @file AGSKMLStyle.h */ //Required for Globals API doc

#import <ArcGIS/AGSObject.h>

NS_ASSUME_NONNULL_BEGIN

@class AGSKMLIconStyle;
@class AGSKMLLabelStyle;
@class AGSKMLLineStyle;
@class AGSKMLPolygonStyle;

/** @brief Specifies the drawing style for a @c AGSKMLNode.

 Controls the drawing style for the icon, line, polygon, and/or label of a KML Node. The style is made
 up of several substyles, including @c AGSKMLIconStyle, @c AGSKMLLineStyle, @c AGSKMLPolygonStyle, and @c AGSKMLLabelStyle.
 Not all node types support styling. For example, an @c AGSKMLTour node cannot be styled.
 @since 100.6
 */
@interface AGSKMLStyle : AGSObject

#pragma mark -
#pragma mark initializers

/** Creates a KML style. Substyles can be modified.
 @since 100.6
 */
+(instancetype)KMLStyle;

#pragma mark -
#pragma mark properties

/** Specifies how icons for point Placemarks are drawn.
 @since 100.6
 */
@property (nullable, nonatomic, strong, readwrite) AGSKMLIconStyle *iconStyle;

/** Specifies how Placemark labels are drawn.
 @since 100.6
 */
@property (nullable, nonatomic, strong, readwrite) AGSKMLLabelStyle *labelStyle;

/** Specifies the drawing style for all lines.
 @since 100.6
 */
@property (nullable, nonatomic, strong, readwrite) AGSKMLLineStyle *lineStyle;

/** Specifies the drawing style for polygons.
 @since 100.6
 */
@property (nullable, nonatomic, strong, readwrite) AGSKMLPolygonStyle *polygonStyle;

#pragma mark -
#pragma mark methods

@end

NS_ASSUME_NONNULL_END
