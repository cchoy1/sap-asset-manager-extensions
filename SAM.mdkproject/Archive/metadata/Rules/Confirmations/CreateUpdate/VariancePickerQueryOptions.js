import QueryBuilder from '../../Common/Query/QueryBuilder';
import libVal from '../../Common/Library/ValidationLibrary';
import libMobile from '../../MobileStatus/MobileStatusLibrary';

export default function VariancePickerQueryOptions(context) {

    let binding = context.getBindingObject();
    let plant = '';
    if (binding.hasOwnProperty('name') && binding.name === 'mConfirmation') {
        binding = libMobile.getWorkOrderHeaderObjFromConfirmationObj(context);
        plant = binding.MainWorkCenterPlant ? binding.MainWorkCenterPlant : binding.PlanningPlant;
    } else {
        plant = binding.WorkOrderHeader.MainWorkCenterPlant ? binding.WorkOrderHeader.MainWorkCenterPlant : binding.WorkOrderHeader.PlanningPlant;
    }	
    let queryBuilder = new QueryBuilder();
    
    if (!libVal.evalIsEmpty(plant)) {
        queryBuilder.addFilter(`Plant eq '${plant}'`);	
    }

    queryBuilder.addExtra('orderby=VarianceReason asc');    
    return queryBuilder.build();
}
