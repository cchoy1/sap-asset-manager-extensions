/**
 * Describe this function...
 * @param {IClientAPI} context
 */
export default function GetPreviousInspectionDate(context) {
	//TODO - Extend OData Service to consume history of asset tag
	// let binding = context.getBindingObject();
	//   if (binding && binding.OrderId) {
	//       return binding.OrderId;
	//   }
	var oPreviousDate = new Date();
         oPreviousDate.setDate(oPreviousDate.getDate() - 10 );
	return oPreviousDate;
}