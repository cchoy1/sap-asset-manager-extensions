export default function PartCreateMaterialUOM(context) {
    if (context.binding['@odata.type'] === context.getGlobalDefinition('/SAPAssetManager/Globals/ODataTypes/BOM.global').getValue()) {
        return context.binding.UoM;
    }
    return context.binding.UnitOfMeasure;
}
