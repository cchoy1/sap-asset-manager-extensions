import libClock from '../../ClockInClockOut/ClockInClockOutLibrary';
import libCommon from '../../Common/Library/CommonLibrary';

export default function ConfirmationSuccess(context) {
    if (libClock.isCICOEnabled(context)) { //Handle removing clock in/out records after time entry
        libCommon.setStateVariable(context, 'ClockTimeSaved', true);
        return libClock.removeUserTimeEntries(context, '', false, true);
    } else {
        return Promise.resolve(true);
    }
}
