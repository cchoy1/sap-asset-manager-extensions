/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

/** @file AGSOfflineMapUpdatesInfo.h */ //Required for Globals API doc

#import <ArcGIS/AGSObject.h>

NS_ASSUME_NONNULL_BEGIN

/** @brief Provides information on the available updates for an offline map.
 
 This type provides high level information on what updates are available for an offline map. Update information
 covers both:
 
 - online changes that can be applied to update your offline map
 - local changes from your offline map that can be sent back to the online services.
 @since 100.6
 */
@interface AGSOfflineMapUpdatesInfo : AGSObject

#pragma mark -
#pragma mark initializers

-(instancetype)init NS_UNAVAILABLE;

#pragma mark -
#pragma mark properties

/** Indicates whether there are changes available to download.
 If this property is @c AGSOfflineUpdateAvailabilityAvailable then there
 are online updates to apply to your offline map.
 
 If this property is @c AGSOfflineUpdateAvailabilityNone then there are no online
 updates available for your offline map.
 
 Note that the availability of updates to download can only be determined for offline maps
 which use the scheduled updates workflow. See @c AGSPreplannedUpdateModeDownloadScheduledUpdates.
 If your offline map is setup to sync changes directly with feature services
 (for example using @c AGSPreplannedUpdateModeSyncWithFeatureServices or created with
 an @c AGSGenerateOfflineMapJob), then this property will be @c AGSOfflineUpdateAvailabilityIndeterminate.
 @since 100.6
 */
@property (nonatomic, assign, readonly) AGSOfflineUpdateAvailability downloadAvailability;

/** Indicates whether the mobile map package must be reopened after applying the available updates.
 In some cases, applying updates may require files, such as mobile geodatabases, to be replaced
 with a new version. When a reopen will be required after updating, this property will be true - see
 @c AGSOfflineMapSyncResult#mobileMapPackageReopenRequired.
 @since 100.6
 */
@property (nonatomic, assign, readonly, getter=isMobileMapPackageReopenRequired) BOOL mobileMapPackageReopenRequired;

/** The total size in bytes of update files to download for a scheduled updates workflow.
 The scheduled updates workflow allows read-only updates to be stored with the online map area
 and downloaded to your device at a later time. Updates can cover several sets of changes to the online geodatabase
 and can cover multiple geodatabases in an offline map. This property is the total download size of
 all files required to update your offline map.
 
 You can use this information to determine whether you want to download updates immediately - for example
 based on available disk space or network availability.
 
 If there are no updates available, or your offline map does not use a scheduled updates workflow, this property
 will be @c 0.
 @since 100.6
 */
@property (nonatomic, assign, readonly) NSInteger scheduledUpdatesDownloadSize;

/** Indicates whether there are local changes to upload.
 If your offline map contains local edits that can be uploaded to
 online feature services, this property will be @c AGSOfflineUpdateAvailabilityAvailable.
 
 If there are no local changes, or your offline map does not support syncing with feature
 services, this property will be @c AGSOfflineUpdateAvailabilityNone.
 @see Geodatabase.hasLocalEdits
 @since 100.6
 */
@property (nonatomic, assign, readonly) AGSOfflineUpdateAvailability uploadAvailability;

#pragma mark -
#pragma mark methods

@end

NS_ASSUME_NONNULL_END
