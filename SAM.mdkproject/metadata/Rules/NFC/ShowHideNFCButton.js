export default function ShowHideNFCButton(context) {

	//CC- clear isScan clientdata viariable for scan.page
	let oOperationPageClientData = context.evaluateTargetPathForAPI('#Page:WorkOrderOperationDetailsPage').getClientData();

	if (oOperationPageClientData) {
		//TODO - Check whether operation is complete, check whether NFC can has been complete and return false to hide button 

		if (oOperationPageClientData.bScanAssetEnabled === true) {
			return false;
		} else {
			return true;
		}
	} else {
		return true;
	}

};