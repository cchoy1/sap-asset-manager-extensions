//
//  AnalyticsDelegate.h
//  AnalyticsFramework
//
//  Created by Patel, Hitesh on 1/26/17.
//  Copyright © 2017 SAP. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 Dummy delegate class, meant for compilation
 */
@interface AnalyticsDelegate : NSObject
-(void) getObjects: (NSDictionary *) dictionary type: (NSString *) type;
@end
