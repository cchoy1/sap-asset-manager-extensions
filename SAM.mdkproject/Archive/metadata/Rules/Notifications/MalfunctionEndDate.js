import libCom from '../Common/Library/CommonLibrary';
import ODataDate from '../Common/Date/ODataDate';

export default function MalfunctionEndDate(context) {
 
    let endDate = libCom.getControlProxy(context, 'MalfunctionEndDatePicker').getValue();
    let end = libCom.getControlProxy(context,'MalfunctionEndTimePicker').getValue();

    end.setFullYear(endDate.getFullYear());
    end.setMonth(endDate.getMonth());
    end.setDate(endDate.getDate());
    let date = new Date(end);
    let odataDate = new ODataDate(date);
    return odataDate.toDBDateString(context);

}
