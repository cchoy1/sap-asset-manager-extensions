/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

/** @file AGSUtilityNetwork.h */ //Required for Globals API doc

#import <ArcGIS/AGSLoadableBase.h>
#import <ArcGIS/AGSRemoteResource.h>

NS_ASSUME_NONNULL_BEGIN

@class AGSArcGISFeature;
@class AGSMap;
@class AGSUtilityAssetType;
@class AGSUtilityElement;
@class AGSUtilityNetworkDefinition;
@class AGSUtilityTerminal;
@class AGSUtilityTraceParameters;
@class AGSUtilityTraceResult;

/** @brief A utility network
 
 This is the central class for utility network schema information and
 tracing. @c AGSUtilityNetwork follows the @c AGSLoadable pattern. When it
 loads, it is populated with the utility network schema. This class
 provides methods to create @c AGSUtilityElement objects for the @c AGSUtilityNetwork.
 @since 100.6
 */
@interface AGSUtilityNetwork : AGSLoadableBase <AGSRemoteResource>

#pragma mark -
#pragma mark initializers

-(instancetype)init NS_UNAVAILABLE;

/** Creates an @c AGSUtilityNetwork using the URL to the feature service
 @param URL The URL to the feature service
 @since 100.6
 */
-(instancetype)initWithURL:(NSURL *)URL;

/** Creates an @c AGSUtilityNetwork using the URL to the feature service
 @param URL The URL to the feature service
 @since 100.6
 */
+(instancetype)utilityNetworkWithURL:(NSURL *)URL;

/** Creates a utility network with the URL to the feature service and a map
 Creates a utility network associated with a particular service, using the same @c AGSArcGISFeatureTable
 objects in use by @c AGSFeatureLayer objects within the map.  This lets any @c AGSUtilityElement or
 @c AGSArcGISFeature objects the @c AGSUtilityNetwork creates or uses be associated with those existing
 tables and layers.  Usually used when instantiating an @c AGSUtilityNetwork object from a web map.
 @param URL The URL to the Feature Service
 @param map A Map that provides FeatureTables to be reused by the utility network
 @since 100.6
 */
-(instancetype)initWithURL:(NSURL *)URL
                       map:(AGSMap *)map;

/** Creates a utility network with the URL to the feature service and a map
 Creates a utility network associated with a particular service, using the same @c AGSArcGISFeatureTable
 objects in use by @c AGSFeatureLayer objects within the map.  This lets any @c AGSUtilityElement or
 @c AGSArcGISFeature objects the @c AGSUtilityNetwork creates or uses be associated with those existing
 tables and layers.  Usually used when instantiating an @c AGSUtilityNetwork object from a web map.
 @param URL The URL to the Feature Service
 @param map A Map that provides FeatureTables to be reused by the utility network
 @since 100.6
 */
+(instancetype)utilityNetworkWithURL:(NSURL *)URL
                                 map:(AGSMap *)map;

#pragma mark -
#pragma mark properties

/** The definition of the @c AGSUtilityNetwork
 @see @c AGSUtilityNetworkDefinition
 @since 100.6
 */
@property (nonatomic, strong, readonly) AGSUtilityNetworkDefinition *definition;

#pragma mark -
#pragma mark methods

/** Creates an @c AGSUtilityElement from an @c AGSUtilityAssetType, a GlobalID, and an optional @c AGSUtilityTerminal.
 If the optional @c AGSUtilityTerminal is not supplied, and the @c AGSUtilityAssetType supports an
 @c AGSUtilityTerminalConfiguration, a default @c AGSUtilityTerminal will be assigned.
 @param assetType The @c AGSUtilityAssetType of the feature from which this feature element is created
 @param globalID The GlobalID of the feature from which this feature element is created
 @param terminal The @c AGSUtilityTerminal
 @return A new @c AGSUtilityElement object
 @since 100.6
 */
-(nullable AGSUtilityElement *)createElementWithAssetType:(AGSUtilityAssetType *)assetType
                                                 globalID:(NSUUID *)globalID
                                                 terminal:(nullable AGSUtilityTerminal *)terminal;

/** Creates an @c AGSUtilityElement from a feature and an optional @c AGSUtilityTerminal.
 If the optional @c AGSUtilityTerminal is not supplied, and the feature's @c AGSUtilityAssetType supports an
 @c AGSUtilityTerminalConfiguration, a default @c AGSUtilityTerminal will be assigned.
 @param feature The @c AGSArcGISFeature from which the feature element is created
 @param terminal The @c AGSUtilityTerminal
 @return A new @c AGSUtilityElement object
 @since 100.6
 */
-(nullable AGSUtilityElement *)createElementWithFeature:(AGSArcGISFeature *)feature
                                               terminal:(nullable AGSUtilityTerminal *)terminal;

/** Gets an array of loaded @c AGSArcGISFeature objects that each
 correspond to one of an array of @c AGSUtilityElement objects
 @param elements The array of utility elements used to find corresponding features
 @param completion Block that is invoked when the operation finishes. The #result parameter is populated if the operation completed successfully, otherwise the #error parameter is populated.
 @return An operation which can be canceled.
 @since 100.6
 */
-(id<AGSCancelable>)featuresForElements:(NSArray<AGSUtilityElement *> *)elements
                             completion:(void(^)(NSArray<AGSArcGISFeature *> * __nullable result, NSError * __nullable error))completion;

/** Begins a trace with the supplied @c AGSUtilityTraceParameters
 This method returns an operation that supplies an array of @c AGSUtilityTraceResult objects when the
 utility network service finishes the trace.
 @param completion Block that is invoked when the operation finishes. The #result parameter is populated if the operation completed successfully, otherwise the #error parameter is populated.
 @return An operation which can be canceled.
 @since 100.6
 */
-(id<AGSCancelable>)traceWithParameters:(AGSUtilityTraceParameters *)traceParameters
                             completion:(void(^)(NSArray<AGSUtilityTraceResult *> * __nullable result, NSError * __nullable error))completion;

@end

NS_ASSUME_NONNULL_END

