/**
 * Describe this function...
 * @param {IClientAPI} context
 */
import {
	Nfc
} from "nativescript-nfc";
import {
	NfcNdefData
} from "nativescript-nfc";
import {
	NfcTagData
} from "nativescript-nfc";
import {
	GlobalVar
} from "../Common/Library/GlobalCommon";
export default function nfcread(context) {
	let nfcInstance = new Nfc();

	nfcInstance.available().then(function (active) {
		//alert(active ? "NFC Device Enabled: Yes" : "No");
		console.log("NFC Device Available");
	});

	nfcInstance.enabled().then(function (on) {
		//alert(on ? "NFC Device Enabled Yes" : "No");
		console.log("NFC Device Enabled");
	});
	// nfcInstance.setOnTagDiscoveredListener(function (data) {
	// 	console.log("Discovered a tag with ID " + data.id);
	// 	this.lastTagDiscovered = data.id
	// }).then(function () {
	// 	console.log("TAG listener added changed");
	// 	//alert("TAG listener added changed");
	// });

	nfcInstance.setOnNdefDiscoveredListener(function (data) {

		if (data.message) {

			//check if NFC tag is formatted correctly with all fields available
			if (data.message.length >= 3) {
				// CC - if data exists set fields from NFC Tag to page

				//CC - Read the NFC Tag and display fields on Scan UI
				let oControlTagId = context.evaluateTargetPath('#Page:Scan/#Control:NFCTagId');
				oControlTagId.setValue(data.message[0].payloadAsString);

				let oControlEqupment = context.evaluateTargetPath('#Page:Scan/#Control:NFCTagEquipment');
				oControlEqupment.setValue(data.message[1].payloadAsString);

				let oControlIndex = context.evaluateTargetPath('#Page:Scan/#Control:NFCTagIndex');
				oControlIndex.setValue(data.message[2].payloadAsString);

				let clientData = context.evaluateTargetPathForAPI('#ClientData').getClientData();
				clientData.tagAssetId = data.message[0].payloadAsString;
				clientData.tagEquipmentName = data.message[1].payloadAsString;
				clientData.tagIndex = data.message[2].payloadAsString;

				// global.oNFCTag = {
				//   assetId: data.message[0].payloadAsString,
				//   equipmentName: data.message[1].payloadAsString,
				//   index: data.message[2].payloadAsString
				// };
			} else {
				//raise error alert
				alert("NFC tag issue, please reformat & try again");
			}

			// for (let m in data.message) {
			//   let record = data.message[m];
			//   global.nfcTagID = data.message
			//   console.log("Ndef discovered! Message record: " + record.payloadAsString);
			//   shwMsg = shwMsg + '\n' + record.payloadAsString;
			// }
			// return shwMsg;
			// //alert("Ndef discovered! Message record: " + shwMsg);
		}
	}, {
		// iOS-specific options
		stopAfterFirstRead: true,
		scanHint: "Scan asset to read NFC tag"

	}).then(function () {
		console.log("OnNdefDiscovered listener added changed");
		//alert("OnNdefDiscovered listener added changed");
	});
}