import {ControlDelegate} from '../Common/ControlDelegate';
declare var com;

export class Analytics {

    // Returns a view
    public create(params, dataService, analyticsExtension): any {
        let analyticsManager = new com.sap.sam.android.plugin.analytics.AnalyticsManager();
        analyticsExtension._bridge = analyticsManager;
        let delegate = ControlDelegate.initWithDataServiceAndBridge(dataService, analyticsManager, analyticsExtension);
        return analyticsManager.create(analyticsExtension.androidContext(), JSON.stringify(params), delegate); 
    }
};
