import libCom from '../Common/Library/CommonLibrary';
import ODataDate from '../Common/Date/ODataDate';

export default function MalfunctionEndTime(context) {

    let date = libCom.getControlProxy(context,'MalfunctionEndTimePicker').getValue();
    date.setSeconds(0);
    let odataDate = new ODataDate(date);
    return odataDate.toDBTimeString(context);
 
}
