import libMobile from '../../MobileStatus/MobileStatusLibrary';
import libCommon from '../../Common/Library/CommonLibrary';
import libClock from '../../ClockInClockOut/ClockInClockOutLibrary';

export default function SubOperationEnableMobileStatus(context) {

    //We don't allow local mobile status changes if App Parameter MOBILESTATUS - EnableOnLocalBusinessObjects = N
    let isLocal = libCommon.isCurrentReadLinkLocal(context.binding['@odata.readLink']);
    if (isLocal) {
        if (!libCommon.isAppParameterEnabled(context, 'MOBILESTATUS', 'EnableOnLocalBusinessObjects')) {
            return false;
        }
    }

    var started = libCommon.getAppParam(context, 'MOBILESTATUS', context.getGlobalDefinition('/SAPAssetManager/Globals/MobileStatus/ParameterNames/StartParameterName.global').getValue());
    var transfer = libCommon.getAppParam(context, 'MOBILESTATUS', context.getGlobalDefinition('/SAPAssetManager/Globals/MobileStatus/ParameterNames/TransferParameterName.global').getValue());
    var complete = libCommon.getAppParam(context, 'MOBILESTATUS', context.getGlobalDefinition('/SAPAssetManager/Globals/MobileStatus/ParameterNames/CompleteParameterName.global').getValue());
    let mobileStatus = libMobile.getMobileStatus(context.getBindingObject());
    var userGUID = libCommon.getUserGuid(context);

    if (libMobile.isSubOperationStatusChangeable(context)) {
        return libClock.isUserClockedIn(context).then(clockedIn => {
            if (mobileStatus === transfer || mobileStatus === complete) {
                return false;
            } else if (mobileStatus === started && libClock.isCICOEnabled(context)) {
                //Started, but I am either not clocked in, or clocked in and this sub-op was started by me
                if (!clockedIn || context.getClientData().mobileUserGUID === userGUID) {
                    return true;
                }
                return false;
            }
            //Clock in/out is disabled. Mobile status can be started, hold, or received.
            if (mobileStatus === started) {
                return true;
            } else {
                let isAnyOtherWorkOrderStartedPromise = context.count('/SAPAssetManager/Services/AssetManager.service', 'MyWorkOrderSubOperations', `$filter=SubOpMobileStatus_Nav/MobileStatus eq '${started}'`);
                return isAnyOtherWorkOrderStartedPromise.then(isAnyOperationStarted => {
                    if (isAnyOperationStarted > 0 && libCommon.isCurrentReadLinkLocal(context.binding['@odata.readLink'])) {
                        return false;
                    } else {
                        return true;
                    }
                });
            }
        });
    } else if (libMobile.isOperationStatusChangeable(context)) {
        //Enable sub-operation status changes only if operation is started.
        let operationObj = context.getBindingObject().WorkOrderOperation;
        if (operationObj) {
            mobileStatus = libMobile.getMobileStatus(operationObj);
            return mobileStatus === started;
        }
    } else {
        let headerMobileStatus = libMobile.getMobileStatus(context.binding.WorkOrderOperation.WOHeader);
        return headerMobileStatus === started;
    }
}
