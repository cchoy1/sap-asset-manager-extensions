export class HierarchyControlDelegate extends NSObject {
    // selector will be exposed so it can be called from native.
    /* tslint:disable */
    public static ObjCExposedMethods = {
        getObjects: { params: [NSString, NSString], returns: interop.types.void },
        runAction: { params: [NSString, NSString], returns: interop.types.void }
    };
    /* tslint:enable */

    public static initWithDataServiceAndBridge(dataService, bridge, controlExtension): HierarchyControlDelegate {
        let controlDelegate = <HierarchyControlDelegate> HierarchyControlDelegate.new();
        controlDelegate._dataService = dataService;
        controlDelegate._bridge = bridge;
        controlDelegate._controlExtension = controlExtension;
        return controlDelegate;
    }

    private _dataService: any;
    private _bridge: any;
    private _controlExtension: any;

    /**
     * Explicitly set reference to control extension
     * @param controlExtension 
     */
    public setControlExtension(controlExtension) {
        this._controlExtension = controlExtension;
    }

    public getObjects(dictionary, type) {
        try {
            this.fetchBusinessObjects(dictionary, type);
        } catch (e) {
            console.log(e);
        }
    }
    
    public runAction(actionInfoJsonString, type) {
        let actionInfoJson = JSON.parse(actionInfoJsonString);
        this._controlExtension.runActionWithInfoAndService(actionInfoJson, type, this._dataService);
    }

    protected fetchBusinessObjects(dictionary, type) {
        if (this._controlExtension) {
            this._controlExtension.getObjects(dictionary, type);
        }
    }
}
