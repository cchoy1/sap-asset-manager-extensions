export default function breakdownOnChange(context) {

    let toggle = false;

    if (context.getValue() === true) {
        toggle = true;
    }
    context.getPageProxy().evaluateTargetPath('#Control:MalfunctionStartDatePicker').setVisible(toggle);
    context.getPageProxy().evaluateTargetPath('#Control:MalfunctionStartTimePicker').setVisible(toggle);

}
