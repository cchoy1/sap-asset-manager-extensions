import isAndroid from '../Common/IsAndroid';

export default function AnalyticsDimensionRatio(context) {
    if (isAndroid(context)) {
        return '420:238';
    } 
    return '';
}
