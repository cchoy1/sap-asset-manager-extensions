/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

/** @file AGSVoiceGuidance.h */ //Required for Globals API doc

#import <ArcGIS/AGSObject.h>

NS_ASSUME_NONNULL_BEGIN

/** @brief Direction guidance text reformatted for speech

 Contains voice guidance and type.
 @since 100.6
 */
@interface AGSVoiceGuidance : AGSObject

#pragma mark -
#pragma mark initializers

-(instancetype)init NS_UNAVAILABLE;

#pragma mark -
#pragma mark properties

/** Voice guidance
 String representation of voice guidance text instruction.
This text can be passed to a text to speech engine.
 @since 100.6
 */
@property (nonatomic, copy, readonly) NSString *text;

/** Voice guidance notification type
 @see @c AGSVoiceGuidanceType
 @since 100.6
 */
@property (nonatomic, assign, readonly) AGSVoiceGuidanceType type;

#pragma mark -
#pragma mark methods

@end

NS_ASSUME_NONNULL_END
