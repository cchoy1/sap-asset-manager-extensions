/*
 COPYRIGHT 2018 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

#import <ArcGIS/AGSObject.h>

@class AGSGeometry;

/** @file AGSKMLGeometry.h */ //Required for Globals API doc

/** @brief A KML Geometry element
 
 An instance of this class represents a KML `<Geometry>` element which could be a Point, LineString,
 LinearRing or Polygon. For MultiGeometry, see `AGSKMLPlacemark#geometries`.
 @since 100.4
 */
@interface AGSKMLGeometry : AGSObject

NS_ASSUME_NONNULL_BEGIN

#pragma mark -
#pragma mark initializers

- (instancetype)init NS_UNAVAILABLE;

/** Initialize a new KML geometry. @c AGSMultipoint, multipart @c AGSPolygon and multipart @c AGSPolyline geometries are not supported.
 @param geometry specifying the location and shape of the KML geometry
 @param altitudeMode specifying how the altitude contained in the geometry's z-value is interpreted
 @return A new initialized KML geometry or nil if an unsupported geometry is passed.
 @since 100.6
 */
-(nullable instancetype)initWithGeometry:(AGSGeometry *)geometry
                            altitudeMode:(AGSKMLAltitudeMode)altitudeMode;

/** Initialize a new KML geometry. @c AGSMultipoint, multipart @c AGSPolygon and multipart @c AGSPolyline geometries are not supported.
 @param geometry specifying the location and shape of the KML geometry
 @param altitudeMode specifying how the altitude contained in the geometry's z-value is interpreted
 @return A new initialized KML geometry or nil if an unsupported geometry is passed.
 @since 100.6
 */
+(nullable instancetype)KMLGeometryWithGeometry:(AGSGeometry *)geometry
                                   altitudeMode:(AGSKMLAltitudeMode)altitudeMode;

/** Initialize a new KML geometry. @c AGSMultipoint, multipart @c AGSPolygon and multipart @c AGSPolyline geometries are not supported.
 @param geometry specifying the location and shape of the KML geometry
 @param altitudeMode specifying how the altitude contained in the geometry's z-value is interpreted
 @param extruded specifying whether to extrude the geometry. i.e connect it to the ground in a 3D view.
 @return A new initialized KML geometry or nil if an unsupported geometry is passed.
 @since 100.6
 */
-(nullable instancetype)initWithGeometry:(AGSGeometry *)geometry
                            altitudeMode:(AGSKMLAltitudeMode)altitudeMode
                                extruded:(BOOL)extruded;

/** Initialize a new KML geometry. @c AGSMultipoint, multipart @c AGSPolygon and multipart @c AGSPolyline geometries are not supported.
 @param geometry specifying the location and shape of the KML geometry
 @param altitudeMode specifying how the altitude contained in the geometry's z-value is interpreted
 @param extruded specifying whether to extrude the geometry. i.e connect it to the ground in a 3D view.
 @return A new initialized KML geometry or nil if an unsupported geometry is passed.
 @since 100.6
 */
+(nullable instancetype)KMLGeometryWithGeometry:(AGSGeometry *)geometry
                                   altitudeMode:(AGSKMLAltitudeMode)altitudeMode
                                       extruded:(BOOL)extruded;

/** Initialize a new KML geometry. @c AGSMultipoint, multipart @c AGSPolygon and multipart @c AGSPolyline geometries are not supported.
 @param geometry specifying the location and shape of the KML geometry
 @param altitudeMode specifying how the altitude contained in the geometry's z-value is interpreted
 @param extruded specifying whether to extrude the geometry. i.e connect it to the ground in a 3D view.
 @param tessellated specifying whether to tessellate the geometry.
 @return A new initialized KML geometry or nil if an unsupported geometry is passed.
 @note Only polyline geometries can be tessellated.
 @since 100.6
 */
-(nullable instancetype)initWithGeometry:(AGSGeometry *)geometry
                            altitudeMode:(AGSKMLAltitudeMode)altitudeMode
                                extruded:(BOOL)extruded
                             tessellated:(BOOL)tessellated;

/** Initialize a new KML geometry. @c AGSMultipoint, multipart @c AGSPolygon and multipart @c AGSPolyline geometries are not supported.
 @param geometry specifying the location and shape of the KML geometry
 @param altitudeMode specifying how the altitude contained in the geometry's z-value is interpreted
 @param extruded specifying whether to extrude the geometry. i.e connect it to the ground in a 3D view.
 @param tessellated specifying whether to tessellate the geometry.
 @return A new initialized KML geometry or nil if an unsupported geometry is passed.
 @note Only polyline geometries can be tessellated.
 @since 100.6
 */
+(nullable instancetype)KMLGeometryWithGeometry:(AGSGeometry *)geometry
                                   altitudeMode:(AGSKMLAltitudeMode)altitudeMode
                                       extruded:(BOOL)extruded
                                    tessellated:(BOOL)tessellated;

#pragma mark -
#pragma mark properties

/** The underlying geometry.
 @since 100.4
 */
@property (nonatomic, strong, readonly) AGSGeometry *geometry;

/** Specifies how the altitude contained in the geometry's z-value is interpreted.
 @since 100.4
 */
@property (nonatomic, assign, readonly) AGSKMLAltitudeMode altitudeMode;

/** The KML geometry type.
 @since 100.4
 */
@property (nonatomic, assign, readonly) AGSKMLGeometryType type;

/** Specifies whether to extrude the geometry, i.e connect it to the ground in a 3D view.
 @since 100.4
 */
@property (nonatomic, assign, readonly, getter=isExtruded) BOOL extruded;

/** Specifies whether to tessellate the geometry. Tessellated geometry can follow the terrain.
 @note In order for tessellated geometry to follow the terrain, the `#altitudeMode` must be set to `AGSKMLAltitudeModeClampToGround`. Only polyline geometries can be tessellated.
 @since 100.4
 */
@property (nonatomic, assign, readonly, getter=isTessellated) BOOL tessellated;

#pragma mark -
#pragma mark methods

NS_ASSUME_NONNULL_END

@end
