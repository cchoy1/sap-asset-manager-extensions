import libCommon from '../../Common/Library/CommonLibrary';
export default function MeasuringPointFDCEntitySet(context) {
    let pageProxy = context.getPageProxy();
    if (libCommon.isDefined(pageProxy.binding)) {
        let odataType = pageProxy.binding['@odata.type'];
        let operation = context.getGlobalDefinition('/SAPAssetManager/Globals/ODataTypes/WorkOrderOperation.global').getValue();
        let equipment = context.getGlobalDefinition('/SAPAssetManager/Globals/ODataTypes/Equipment.global').getValue();
        let floc = context.getGlobalDefinition('/SAPAssetManager/Globals/ODataTypes/FunctionalLocation.global').getValue();
        switch (odataType) {
            case operation:
              return "$filter=(PRTCategory eq 'P')&$expand=PRTPoint/MeasurementDocs,PRTPoint/MeasurementDocs/MeasuringPoint&$orderby=PRTPoint/SortField&$select=PRTPoint/Point,PRTPoint/PointDesc,PRTPoint/UoM,PRTPoint/RangeUOM," +
              'PRTPoint/CounterOverflow,PRTPoint/IsCounter,PRTPoint/CodeGroup,PRTPoint/CatalogType,PRTPoint/IsReverse,PRTPoint/IsLowerRange,PRTPoint/IsLowerRange,PRTPoint/IsUpperRange,PRTPoint/IsCodeSufficient,PRTPoint/LowerRange,PRTPoint/UpperRange,PRTPoint/Decimal,PRTPoint/CharName,PRTPoint/IsCounterOverflow,'+
              'PRTPoint/MeasurementDocs/ReadingDate,PRTPoint/MeasurementDocs/ReadingTime,PRTPoint/MeasurementDocs/CodeGroup,PRTPoint/MeasurementDocs/ValuationCode,PRTPoint/MeasurementDocs/CodeShortText,PRTPoint/MeasurementDocs/ReadingValue,PRTPoint/MeasurementDocs/IsCounterReading,PRTPoint/MeasurementDocs/IsCounterReading,PRTPoint/MeasurementDocs/CounterReadingDifference,PRTPoint/MeasurementDocs/MeasurementDocNum,PRTPoint/MeasurementDocs/MeasuringPoint/CharName,PRTPoint/MeasurementDocs/MeasuringPoint/IsCounter';
            case equipment:
              return '$orderby=SortField';
            case floc:
              return '$orderby=SortField';
            default:
              return '';
        }
    }

}
