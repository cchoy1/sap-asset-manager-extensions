import { BaseExtension } from './BaseExtension';
import { AnalyticsBridge } from 'extension-Analytics';
import { AnalyticsParser } from './AnalyticsParser';
import { ObservableArray } from 'tns-core-modules/data/observable-array';
import { Utils } from './Utils';
import { device } from 'tns-core-modules/platform';

export class AnalyticsViewExtension extends BaseExtension {

    public initialize(props) {
        this._parser = new AnalyticsParser();   
        super.initialize(props);
        if (this.context && this.context.binding && (this.context.binding instanceof ObservableArray)) {
            let bindingObject = this.context.binding.getItem(0);
            this.context.binding = bindingObject;
        }     
        this.setViewController(this.createAnalyticsView(this.getParams()));    
    }

    public createAnalyticsView(params) {
        let analytics: AnalyticsBridge = new AnalyticsBridge();
        if (device.deviceType === 'Phone') {
            params.ViewConfig.Margin = params.ViewConfig.MarginPhone;
        } else {
            params.ViewConfig.Margin = params.ViewConfig.MarginTablet;
        }
        // Control has to be set to a chart type by default and chart will update accordingly on the callback 
        params.ViewConfig.ChartType = 'line';
        let vc = analytics.create(params, this.getDataService(), this);
        return vc;      
    }
    
    public getObjects(jsonDictionary, type) {
        super.getObjects(jsonDictionary, type).then(entries => {
            let entitySet = jsonDictionary.EntitySet;  
            let bo = this.getBusinessObject(entitySet);
            bo.ChartData.Title = this.getTitle(bo.ChartData.Title);              
            if (entries[0]) {
                if (entries[0].hasOwnProperty('Filter')) {
                    entries = entries.filter((result) => {
                        return result.Filter === true;
                    });
                }
                if (entries[0].ChartType === 'line') {
                    bo.ChartData.LineChartDataSet.entries = entries;
                    bo.ChartData.ChartType = 'line';               
                }
                if (entries[0].ChartType === 'bar') {
                    entries.sort((a, b) => {
                        return a.x - b.x;
                    });
                    for (let i in entries) {
                      if (entries.hasOwnProperty(i)) {
                        entries[i].x = Number(i);
                      }
                    }
                    bo.ChartData.BarChartDataSet.entries = entries; 
                    bo.ChartData.ChartType = 'bar';             
                } 
                if (entries[0].ChartType === 'valCode') {
                    entries.sort((a, b) => {
                        return b.x - a.x;
                    });
                    bo.ChartData.entries = entries; 
                    bo.ChartData.ChartType = 'valCode';
                    bo.ChartData.ValuationCode = entries[0].ValuationCode;   
                }
            } else {
                // If no data from getObjects set ChartType to line
                bo.ChartData.LineChartDataSet.entries = entries;
                bo.ChartData.ChartType = 'line';
            } 
            if  (Object.keys(bo.ChartData.KPIData).length > 0 && typeof bo.ChartData.KPIData === 'object') {
                    this.getKPIData(bo.ChartData.KPIData).then(kpiData => {
                        if (kpiData && kpiData.metric) {
                            bo.ChartData.KPIData = kpiData;
                            if (device.os === 'Android') {
                                this._bridge.callback(JSON.stringify(bo), type);
                            } else {
                                this._bridge.callback(bo, type);
                            }
                        }
                    });
            } else {
                if (device.os === 'Android') {
                    this._bridge.callback(JSON.stringify(bo), type);
                } else {
                    this._bridge.callback(bo, type);
                }
            }       
        });
    }

    public getTitle(property: string): any {
        return this.bindProperty(property).trim();
    }

    public getKPIData(kpiData): Promise<any> {
        if (kpiData && kpiData.Target && kpiData.Target.Property) {
            let objectsToBind = this.getObjectsToBind(kpiData.Target.Property);
            if (objectsToBind && objectsToBind.length > 0 && objectsToBind[0]) {
                // For KPI Data, we will always take the 1st measuring point in the collection.
                return this.getObject(kpiData, objectsToBind[0]);
            }
        } else {
            return Promise.resolve(null);
        }
    }

    public getBusinessObject(entitySet): any {
        let bo;
        Object.keys(this._params.BusinessObjects).forEach(sKey => {
             let object = Utils.clone(this._params.BusinessObjects[sKey]);
             Object.keys(object).forEach(key => {
                if (key === 'Target') {
                    let target = object[key];
                    Object.keys(target).forEach(targetKey => {
                        if (target[targetKey] === entitySet) {
                            bo = object;
                        }    
                    });
                }    
             });    
        });
        return bo;
    }

    public getObjectSchema(entitySet) {
        let properties: any;
        let bo = this.getBusinessObject(entitySet);
        if (bo) {
            if (bo.ChartData.KPIData) {
                properties = bo.ChartData.KPIData;
            } 
            if (bo.ChartData.entries) {
                properties = bo.ChartData.entries;
            }         
        } else {
            properties = {};
        }
        return properties;
    }

    protected readFromContext() {
        return true;
    }

};
