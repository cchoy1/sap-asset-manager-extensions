/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

/** @file AGSTransformationMatrix.h */ //Required for Globals API doc

#import <ArcGIS/AGSObject.h>

NS_ASSUME_NONNULL_BEGIN

/** @brief A class for holding a translation and quaternion array. This is used for camera movement while preventing gimbal lock.

 @since 100.6
 */
@interface AGSTransformationMatrix : AGSObject

#pragma mark -
#pragma mark initializers

-(instancetype)init NS_UNAVAILABLE;

#pragma mark -
#pragma mark properties

/** The w quaternion of the transformation matrix.
 @since 100.6
 */
@property (nonatomic, assign, readonly) double quaternionW;

/** The x quaternion of the transformation matrix.
 @since 100.6
 */
@property (nonatomic, assign, readonly) double quaternionX;

/** The y quaternion of the transformation matrix.
 @since 100.6
 */
@property (nonatomic, assign, readonly) double quaternionY;

/** The z quaternion of the transformation matrix.
 @since 100.6
 */
@property (nonatomic, assign, readonly) double quaternionZ;

/** The x translation of the transformation matrix.
 @since 100.6
 */
@property (nonatomic, assign, readonly) double translationX;

/** The y translation of the transformation matrix.
 @since 100.6
 */
@property (nonatomic, assign, readonly) double translationY;

/** The z translation of the transformation matrix.
 @since 100.6
 */
@property (nonatomic, assign, readonly) double translationZ;

/** Creates an @c AGSTransformationMatrix object with an identity transform.
 Subtracting an @c AGSTransformationMatrix from an identity matrix is useful for getting the
 inverse of that transformation matrix, i.e. identity matrix - other matrix = inverse(other matrix).
 @return An @c AGSTransformationMatrix.
 @since 100.6
 */
@property (class, strong, readonly) AGSTransformationMatrix *identityMatrix;

#pragma mark -
#pragma mark methods

/** Create an @c AGSTransformationMatrix object using x, y, z, w quaternion and x, y, z translations.
 @param quaternionX The x quaternion of the transformation matrix.
 @param quaternionY The y quaternion of the transformation matrix.
 @param quaternionZ The z quaternion of the transformation matrix.
 @param quaternionW The w quaternion of the transformation matrix.
 @param translationX The x position of the transformation matrix.
 @param translationY The y position of the transformation matrix.
 @param translationZ The z position of the transformation matrix.
 @since 100.6
 */
+(AGSTransformationMatrix*)transformationMatrixWithQuaternionX:(double)quaternionX
                                                   quaternionY:(double)quaternionY
                                                   quaternionZ:(double)quaternionZ
                                                   quaternionW:(double)quaternionW
                                                  translationX:(double)translationX
                                                  translationY:(double)translationY
                                                  translationZ:(double)translationZ;

/** Adds this and transformation together and returns the result. return = (this + parameter)
 @param transformation The @c AGSTransformationMatrix to be added onto this @c AGSTransformationMatrix.
 @return A new @c AGSTransformationMatrix object which is the result of adding two @c AGSTransformationMatrix
 @since 100.6
 */
-(AGSTransformationMatrix *)addTransformation:(AGSTransformationMatrix *)transformation;

/** Subtracts the parameter from this object and returns the result. return = (this - parameter)
 @param transformation The @c AGSTransformationMatrix to be subtracted from this @c AGSTransformationMatrix.
 @return A new @c AGSTransformationMatrix object which is the result of subtracting two @c AGSTransformationMatrix
 @since 100.6
 */
-(AGSTransformationMatrix *)subtractTransformation:(AGSTransformationMatrix *)transformation;

@end

NS_ASSUME_NONNULL_END
