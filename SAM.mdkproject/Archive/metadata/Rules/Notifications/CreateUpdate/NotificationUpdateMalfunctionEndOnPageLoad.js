import OffsetODataDate from '../../Common/Date/OffsetODataDate';

export default function NotificationUpdateMalfunctionEndOnPageLoad(context) {
    
    let binding = context.getBindingObject();
    let formCellContainer = context.getControl('FormCellContainer');

    //Malfunction date/time
    let startDate = formCellContainer.getControl('MalfunctionStartDatePicker');
    let startTime = formCellContainer.getControl('MalfunctionStartTimePicker');

    startDate.setValue(new OffsetODataDate(context, binding.MalfunctionStartDate, binding.MalfunctionStartTime).date());
    startTime.setValue(new OffsetODataDate(context, binding.MalfunctionStartDate, binding.MalfunctionStartTime).date());
 
}

