import libCommon from '../Common/Library/CommonLibrary';

export default function WorkOrderListViewSetCaption(context) {

    let queryOption = libCommon.getStateVariable(context,'WORKORDER_FILTER');

    var params = [];
    let totalCountPromise = context.count('/SAPAssetManager/Services/AssetManager.service','MyWorkOrderHeaders', '');
    let countPromise = context.count('/SAPAssetManager/Services/AssetManager.service','MyWorkOrderHeaders',queryOption);

    return Promise.all([totalCountPromise, countPromise]).then(function(resultsArray) {
        let totalCount = resultsArray[0];
        let count = resultsArray[1];
        params.push(count);
        params.push(totalCount);
        if (count === totalCount) {
            return context.setCaption(context.localizeText('work_order_x', [totalCount]));
        }
        return context.setCaption(context.localizeText('work_order_x_x', params));
    });
}
