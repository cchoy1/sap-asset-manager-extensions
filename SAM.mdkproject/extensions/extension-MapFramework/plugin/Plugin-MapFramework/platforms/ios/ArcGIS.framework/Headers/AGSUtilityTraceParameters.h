/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

/** @file AGSUtilityTraceParameters.h */ //Required for Globals API doc

#import <ArcGIS/AGSObject.h>

NS_ASSUME_NONNULL_BEGIN

@class AGSUtilityElement;

/** @brief Utility network trace parameters

 @since 100.6
 */
@interface AGSUtilityTraceParameters : AGSObject

#pragma mark -
#pragma mark initializers

-(instancetype)init NS_UNAVAILABLE;

/** Constructs a new @c AGSUtilityTraceParameters for the specified trace type and
 with the supplied starting locations
 @param traceType The type of trace to perform
 @param startingLocations The collection of starting locations
 @since 100.6
 */
-(instancetype)initWithTraceType:(AGSUtilityTraceType)traceType 
               startingLocations:(NSArray<AGSUtilityElement *> *)startingLocations;

/** Constructs a new @c AGSUtilityTraceParameters for the specified trace type and
 with the supplied starting locations
 @param traceType The type of trace to perform
 @param startingLocations The collection of starting locations
 @since 100.6
 */
+(instancetype)utilityTraceParametersWithTraceType:(AGSUtilityTraceType)traceType 
                                 startingLocations:(NSArray<AGSUtilityElement *> *)startingLocations;

#pragma mark -
#pragma mark properties

/** The array of barriers
 @since 100.6
 */
@property (nonatomic, copy, readwrite) NSArray<AGSUtilityElement *> *barriers;

/** The array of result types
 @since 100.6
 */
@property (nonatomic, copy, readwrite) NSArray<NSValue *> *resultTypes;

/** The array of starting locations
 @since 100.6
 */
@property (nonatomic, copy, readwrite) NSArray<AGSUtilityElement *> *startingLocations;

/** The trace type
 @since 100.6
 */
@property (nonatomic, assign, readonly) AGSUtilityTraceType traceType;

#pragma mark -
#pragma mark methods

@end

NS_ASSUME_NONNULL_END
