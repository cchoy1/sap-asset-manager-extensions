import libCommon from '../Common/Library/CommonLibrary';
import Constants from '../Common/Library/ConstantsLibrary';
import libNotifStatus from './MobileStatus/NotificationMobileStatusLibrary';
import OffsetODataDate from '../Common/Date/OffsetODataDate';

export default function NotificationUpdateNav(context) {
    let currentReadLink = libCommon.getTargetPathValue(context, '#Property:@odata.readLink');
    let isLocal = libCommon.isCurrentReadLinkLocal(currentReadLink);
    let binding = context.getBindingObject();

    binding._MalfunctionStartDate = '';
    binding._MalfunctionStartTime = '';

    if (binding.BreakdownIndicator && binding.MalfunctionStartDate) { //Set up malfunction date and time if necessary
        binding._MalfunctionStartDate = new OffsetODataDate(context, binding.MalfunctionStartDate, binding.MalfunctionStartTime).date();
        binding._MalfunctionStartTime = binding._MalfunctionStartDate;
    }
    
    context.setActionBinding(binding);

    if (!isLocal) {
        return libNotifStatus.isNotificationComplete(context).then(status => {
            if (!status) {
                libCommon.setOnCreateUpdateFlag(context, Constants.updateFlag);
                return context.executeAction('/SAPAssetManager/Actions/Notifications/CreateUpdate/NotificationCreateUpdateNav.action');
            }
            return '';
        });
    }
    libCommon.setOnCreateUpdateFlag(context, Constants.updateFlag);
    return context.executeAction('/SAPAssetManager/Actions/Notifications/CreateUpdate/NotificationCreateUpdateNav.action');
}
