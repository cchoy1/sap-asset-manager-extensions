import { BaseExtension } from './BaseExtension';
import { HierarchyControl } from 'extension-HierarchyFramework';
import { HierarchyParser } from './HierarchyParser';
import { IDataService } from '../../../data/IDataService';
import { Utils } from './Utils';
import { EventHandler } from '../../../EventHandler';
import { ITargetServiceSpecifier } from '../../../data/ITargetSpecifier';
import { I18nHelper } from '../../../utils/I18nHelper';
import { device } from 'tns-core-modules/platform';
import { Context } from '../../../context/Context';

export class HierarchyExtension extends BaseExtension {
    public selectedId: any;
    protected _root: any;

    public initialize(props) {
        this._parser = new HierarchyParser();
        super.initialize(props); 
        this.addRootObjectToParams();
        let hc: HierarchyControl = new HierarchyControl();
        let vc = hc.create(this.getParams(), this.getDataService(), this);
        this.setViewController(vc);
    }

    get isBindable(): boolean {
        return true;
    }
    public bind(): Promise<any> {
        return this.observable().bindValue(this.definition().getValue());
    }

    public onDataChanged(action: any, result: any) {
        this.runCallback({}, 'DataChanged');
    }

    public getObjectSync(schema, item): any {
        let object = Utils.clone(schema);
        let boundObject = this.bindParametersSync(item, object);
        return boundObject;
     }

    public bindParametersSync(object, params): any {
        let context: Context = new Context(object);
        if (params) {
            Object.keys(params).forEach(sKey => {
                let value = params[sKey];
                if (value) {
                    if (typeof (value) !== 'object') {
                        // if rule, then skip
                        if (typeof(value) !== 'string' || value.indexOf('/Rules/') < 0) {
                            params[sKey] = this._parser.parseValue(value, context);
                        }
                    } else {
                        params[sKey] = this.bindParametersSync(object, value);
                    }
                }
            });
        }
        return params;
    }

    public getObjects(entityId, type) {
        let targets = [];
        const jsonDictionary = JSON.parse(entityId);
        const eid = jsonDictionary.ID;
        switch (type) {
            case 'Children':
                targets = jsonDictionary.Children;
                break;
            case 'ChildCount':
                let rule = jsonDictionary.ChildCountQuery;
                this.executeActionOrRule(rule, new Context(jsonDictionary)).then(result => {
                    this.runCallback({ChildCount: result, ID: eid}, type);
                });
                return;
            case 'Parent':
                targets = jsonDictionary.Parent;
                break;
            case 'FetchObject':
                targets.push(jsonDictionary);
                break;
            default:
                console.log('no object available of type ' + type);
                return;
        }
        this.processQueries(targets, eid, type).then(result => {
            if (   type === 'Parent' 
                && result 
                && result.length > 0 
                && result[0].Properties.ChildCount === -1 
                && result[0].Properties.ChildCountQuery) {
                // for parent objects that would normally defer their childcounts, retrieve them instead
                this.executeActionOrRule(result[0].Properties.ChildCountQuery, 
                                         new Context(result[0].Properties)).then(childCount => {
                    result[0].Properties.ChildCount = childCount;
                    this.runCallback({Objects: result, ID: eid}, type);
                });
            } else {
                this.runCallback({Objects: result, ID: eid}, type);
            }
        });
    }

    public processQueries(queries: Object[], eid: string, type: string): Promise<any> {
        return this.runQueries(queries, eid, type).then(results => {
            let arrObjects = [];
            if (results.length > 0) {
                results.forEach(result => {
                    if (result.length > 0) {
                        result.forEach(item => {
                            arrObjects.push(item);
                        });
                    }
               });
            }
            return arrObjects;
        });
    }

    public runCallback(json: any, type: string) {
        console.log(json.ID + ' ' + type);
        if (device.os === 'Android') {
            this._bridge.callback(JSON.stringify(json), type);
        } else {
            this._bridge.callback(json, type);
        }
    }

    public runQueries(targets, eid, type): Promise<any> {
        if (targets.length > 0) {
            let aPromises: Promise<any>[] = [];
            return new Promise((resolve, reject) => {
                targets.forEach(item => {
                    let schema = this.getObjectSchema(item.Target.EntitySet);
                    // for fetch object, we need to update the entityset to the ID, 
                    // so that only that object is returned.
                    if (type === 'FetchObject') {
                        item.Target.EntitySet = eid;
                    }
                    aPromises.push(this.getObjectsFromService(schema, item, ''));
                });
                return Promise.all(aPromises).then(results => {
                    resolve(results);
                });
            });
        } else {
            return Promise.resolve([]);
        }
    }

    private addRootObjectToParams() {
        this._root = this.context.binding;
        let schema = this.getObjectSchema(this.getEntitySetNameFromEntity(this._root['@odata.readLink']));
        let rootBusinessObject = this.getObjectSync(schema, this._root);
        rootBusinessObject.Properties.ChildCount = this.context.binding.HC_ROOT_CHILDCOUNT;
        let params = this.getParams();
        params.Root = rootBusinessObject;
        this._params = params;
    } 

    private getEntitySetNameFromEntity(entity) {
        let match = entity.match(/(.*)(\()/);
        let entitySeName = match[1];
        return entitySeName;
    }
};
