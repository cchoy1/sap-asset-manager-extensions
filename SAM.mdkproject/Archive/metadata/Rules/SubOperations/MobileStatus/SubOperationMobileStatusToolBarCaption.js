import libMobile from '../../MobileStatus/MobileStatusLibrary';
import libCommon from '../../Common/Library/CommonLibrary';
import libSubOPMobile from './SubOperationMobileStatusLibrary';
import libClock from '../../ClockInClockOut/ClockInClockOutLibrary';

export default function SubOperationMobileStatusToolBarCaption(context) {
    let received = libCommon.getAppParam(context, 'MOBILESTATUS', context.getGlobalDefinition('/SAPAssetManager/Globals/MobileStatus/ParameterNames/ReceivedParameterName.global').getValue());
    let hold = libCommon.getAppParam(context, 'MOBILESTATUS', context.getGlobalDefinition('/SAPAssetManager/Globals/MobileStatus/ParameterNames/HoldParameterName.global').getValue());
    let started = libCommon.getAppParam(context, 'MOBILESTATUS', context.getGlobalDefinition('/SAPAssetManager/Globals/MobileStatus/ParameterNames/StartParameterName.global').getValue());
    let transfer = libCommon.getAppParam(context, 'MOBILESTATUS', context.getGlobalDefinition('/SAPAssetManager/Globals/MobileStatus/ParameterNames/TransferParameterName.global').getValue());
    let complete = libCommon.getAppParam(context, 'MOBILESTATUS', context.getGlobalDefinition('/SAPAssetManager/Globals/MobileStatus/ParameterNames/CompleteParameterName.global').getValue());
    let mobileStatus = libMobile.getMobileStatus(context.binding);

    //Change sub-operation status when assignment type is at sub-operation level.
    if (libMobile.isSubOperationStatusChangeable(context)) {

        //User may be clocked in to this Sub-Operation locally regardless of what mobile status is set to
        //Status may have been changed by another user, so trap that here
        if (libClock.isBusinessObjectClockedIn(context)) {
            return context.localizeText('clock_out');
        } else {
            if (mobileStatus === received || mobileStatus === hold) {
                //This sub-operation is not started. It is currently either on hold or received status.
                let isAnyOtherSubOperationStartedPromise = libSubOPMobile.isAnySubOperationStarted(context);
                return isAnyOtherSubOperationStartedPromise.then(
                    isAnyOtherOperationStarted => {
                        if (isAnyOtherOperationStarted) {
                            return context.localizeText('transfer');
                        } else {
                            if (libClock.isCICOEnabled(context)) {
                                return context.localizeText('clock_in');
                            } else {
                                return context.localizeText('start_suboperation');
                            }
                        }
                    }
                );
            } else if (mobileStatus === started) {
                if (libClock.isCICOEnabled(context)) { //Handle clock in/out feature
                    if (context.binding.clockMobileUserGUID === libCommon.getUserGuid(context)) { //This sub-operation was started by current user
                        return context.localizeText('clock_out');
                    } else {
                        return context.localizeText('clock_in'); //This sub-operation was started by someone else, so current user can also start it
                    }
                } else {
                    return context.localizeText('end_suboperation');
                }
            } else if (mobileStatus === transfer) {
                return context.localizeText('transferred');
            } else if (mobileStatus === complete) {
                return context.localizeText('completed');
            }
            return context.getGlobalDefinition('/SAPAssetManager/Globals/MobileStatus/Status.global').getValue();
        }
    } else {
        //Change sub-operation status for all other assignment types.
        return libMobile.isMobileStatusConfirmed(context, context.binding.SubOperationNo).then(result => {
            if (result) {
                return context.localizeText('unconfirm');
            } else {
                return context.localizeText('confirm');
            }
        });
    }
}
