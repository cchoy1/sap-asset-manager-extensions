/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

/** @file AGSUtilityNetworkDefinition.h */ //Required for Globals API doc

#import <ArcGIS/AGSObject.h>

NS_ASSUME_NONNULL_BEGIN

@class AGSUtilityCategory;
@class AGSUtilityNetworkAttribute;
@class AGSUtilityNetworkSource;

/** @brief An object that represents the metadata of a Utility Network feature service.
 
 Provides metadata (e.g domain networks, network sources, and more) about a Utility Network service. You need to pass this object to all Utility Network Definition functions.
 @since 100.6
 */
@interface AGSUtilityNetworkDefinition : AGSObject

#pragma mark -
#pragma mark initializers

-(instancetype)init NS_UNAVAILABLE;

#pragma mark -
#pragma mark properties

/** The collection of utility network categories in the utility network feature service.
 An @c AGSUtilityCategory is used to define a characteristic of an asset in a network. The objects in this collection incorporate semantics of the utility network for subnetwork
 management and tracing operations.
 @since 100.6
 */
@property (nonatomic, copy, readonly) NSArray<AGSUtilityCategory *> *categories;

/** The network attributes in the utility network definition.
 An @c AGSUtilityNetworkAttribute is an attribute that is copied and stored in the topological index. The utility network tracing task can read and make decisions using network attributes that are stored in the topological index.
 @since 100.6
 */
@property (nonatomic, copy, readonly) NSArray<AGSUtilityNetworkAttribute *> *networkAttributes;

/** The collection of network sources in the utility network definition
 @c AGSUtilityNetworkSource objects represent various sources of network information such as structures, lines, junctions, and associations.
 @since 100.6
 */
@property (nonatomic, copy, readonly) NSArray<AGSUtilityNetworkSource *> *networkSources;

/** The utility network schema version number reported by the utility network feature service
 Only certain schema versions are supported by ArcGIS Runtime.
 @since 100.6
 */
@property (nonatomic, assign, readonly) NSInteger schemaVersion;

#pragma mark -
#pragma mark methods

/** Gets the associated network attribute with the specified network attribute name.
 @param name The name of the network attribute.
 @return An @c AGSUtilityNetworkAttribute.
 @since 100.6
 */
-(nullable AGSUtilityNetworkAttribute *)networkAttributeWithName:(NSString *)name;

/** Gets the associated network source with the specified network source name.
 @param name The name of the network source.
 @return An @c AGSUtilityNetworkSource.
 @since 100.6
 */
-(nullable AGSUtilityNetworkSource *)networkSourceWithName:(NSString *)name;

@end

NS_ASSUME_NONNULL_END
