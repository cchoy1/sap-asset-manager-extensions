/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

/** @file AGSUtilityCategory.h */ //Required for Globals API doc

#import <ArcGIS/AGSObject.h>

NS_ASSUME_NONNULL_BEGIN

/** @brief A category used to define a characteristic of an asset in a network
 
 @c AGSUtilityCategory can be used during tracing operations to define constraints or conditions based upon specific characteristics of asset types in the network.
 @since 100.6
 */
@interface AGSUtilityCategory : AGSObject

#pragma mark -
#pragma mark initializers

-(instancetype)init NS_UNAVAILABLE;

#pragma mark -
#pragma mark properties

/** The name of the category
 @since 100.6
 */
@property (nonatomic, copy, readonly) NSString *name;

#pragma mark -
#pragma mark methods

@end

NS_ASSUME_NONNULL_END
