import libCom from '../Common/Library/CommonLibrary';
import ODataDate from '../Common/Date/ODataDate';

export default function MalfunctionStartDate(context) {
 
    let breakdown = libCom.getControlProxy(context,'BreakdownSwitch').getValue();
    let start = null;

    if (breakdown) {
        let startDate = libCom.getControlProxy(context, 'MalfunctionStartDatePicker').getValue();
        start = libCom.getControlProxy(context,'MalfunctionStartTimePicker').getValue();

        start.setFullYear(startDate.getFullYear());
        start.setMonth(startDate.getMonth());
        start.setDate(startDate.getDate());
        let date = new Date(start);
        let odataDate = new ODataDate(date);
        start = odataDate.toDBDateString(context);
    }
    
    return start;
}
