import { BaseExtension } from './BaseExtension';
import { AnalyticsViewExtension } from './AnalyticsViewExtension';
import { AnalyticsBridge } from 'extension-Analytics';
import { AnalyticsParser } from './AnalyticsParser';
import { ObservableArray } from 'tns-core-modules/data/observable-array';
import { Utils } from './Utils';

export class AnalyticsViewObjHeaderExtension extends AnalyticsViewExtension {

    public createAnalyticsView() {
        let params = this.getParams();
        if (this.isAnalyticsDataPresent(params)) {
            return super.createAnalyticsView(params);
        } else {
            return null;
        }
    }

    private isAnalyticsDataPresent(params) {
        let bo = params.BusinessObjects[0];
        let objectsToBind = this.getObjectsToBind(bo.Target.Property);
        return (objectsToBind != null && objectsToBind[0] != null ) ? true : false;
    }
};
