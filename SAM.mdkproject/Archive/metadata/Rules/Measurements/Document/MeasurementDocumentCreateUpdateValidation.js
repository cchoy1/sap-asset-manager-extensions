import libPoint from '../MeasuringPointLibrary';
import libCom from '../../Common/Library/CommonLibrary';
import prevReadingQuery from '../Points/MeasurementDocumentPreviousReadingQuery';

export default function MeasurementDocumentCreateUpdateValidation(pageClientAPI) {

    if (!pageClientAPI) {
        throw new TypeError('Context can\'t be null or undefined');
    }

    let dt = new Date();

    libCom.setStateVariable(pageClientAPI, 'CurrentDateTime', dt);

    //Check field data against business logic here
    //Return true if validation succeeded, or False if failed

    let previousReadingPromise = '';

    if (pageClientAPI.binding['@odata.type'] === '#sap_mobile.MeasuringPoint') {
        previousReadingPromise = libPoint.getLatestNonLocalReading(pageClientAPI, pageClientAPI.binding, prevReadingQuery());
    } else {
        // If MeasuringPoint is undefined, assume we're on a PRT Point
        if (!pageClientAPI.binding.MeasuringPoint) {
            previousReadingPromise = libPoint.getLatestNonLocalReading(pageClientAPI, pageClientAPI.binding.PRTPoint, prevReadingQuery());
        } else {
            previousReadingPromise = libPoint.getLatestNonLocalReading(pageClientAPI, pageClientAPI.binding.MeasuringPoint, prevReadingQuery());
        }
    }

    return previousReadingPromise.then( (previousReading) => {
        if (previousReading && previousReading.length > 0) {
            let binding = pageClientAPI.binding;
            binding.previousReadingObj = previousReading.getItem(0);
            pageClientAPI.setActionBinding(binding);
        }
        return libPoint.measurementDocumentCreateUpdateValidation(pageClientAPI).then(result => {
            return result;
        });
    });
}
