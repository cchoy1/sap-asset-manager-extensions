import {
	Nfc
} from "nativescript-nfc";
import {
	NfcNdefData
} from "nativescript-nfc";
import {
	NfcTagData
} from "nativescript-nfc";

export default function UpdateNFC(context) {
	let nfcInstance = new Nfc();
	
	//CC- clear isScan clientdata viariable for scan.page
	let oOperationPageClientData = context.evaluateTargetPathForAPI('#Page:WorkOrderOperationDetailsPage').getClientData();
	oOperationPageClientData.bScanAssetEnabled = true;
	

	nfcInstance.available().then(function (active) {
		//alert(active ? "NFC Device Enabled: Yes" : "No");
		console.log("NFC Device Available");
	});

	nfcInstance.enabled().then(function (on) {
		//alert(on ? "NFC Device Enabled Yes" : "No");
		console.log("NFC Device Enabled");
	});
	// nfcInstance.setOnTagDiscoveredListener(function (data) {
	// 	console.log("Discovered a tag with ID " + data.id);
	// 	this.lastTagDiscovered = data.id
	// }).then(function () {
	// 	console.log("TAG listener added changed");
	// 	//alert("TAG listener added changed");
	// });

	nfcInstance.setOnNdefDiscoveredListener(function (data) {

		if (data.message) {

			
			} else {
				//raise error alert
				alert("NFC tag issue, please reformat & try again");
			}

			// for (let m in data.message) {
			//   let record = data.message[m];
			//   global.nfcTagID = data.message
			//   console.log("Ndef discovered! Message record: " + record.payloadAsString);
			//   shwMsg = shwMsg + '\n' + record.payloadAsString;
			// }
			// return shwMsg;
			// //alert("Ndef discovered! Message record: " + shwMsg);
		
	}, {
		// iOS-specific options
		stopAfterFirstRead: true,
		scanHint: "Scan to write to NFC tag"

	}).then(function () {
		//CC - once tag has been updated, navigate back to the operations view & set the bScanAssetEnabled field to false to disable scan asset button and enable operation action buttons on operations view
		oOperationPageClientData.bScanAssetEnabled  = false;
		//return context.executeAction('/SAPAssetManager/Actions/NavBackToOperation.action');
		return context.executeAction('/SAPAssetManager/Actions/CloseScanPage.action');
		
	});
	
}