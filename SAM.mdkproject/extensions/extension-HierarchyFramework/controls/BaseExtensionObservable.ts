import { BaseObservable } from '../../../observables/BaseObservable';
import { IControl } from '../../../controls/IControl';
import { ODataAction } from '../../../actions/ODataAction';

export class BaseExtensionObservable extends BaseObservable {
    public constructor(control: IControl) {
        super(control, control.definition(), control.page());
    } 

    public bindValue(value: any): Promise<any> {
        return Promise.resolve();
    }

    public onDataChanged(action: ODataAction, result: any) {
        super.onDataChanged(action, result);
        this.control.onDataChanged(action, result);
      }
};
