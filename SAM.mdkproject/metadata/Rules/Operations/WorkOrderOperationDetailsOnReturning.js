import toolbarCaption from './MobileStatus/OperationMobileStatusToolBarCaption';
import CommonLibrary from '../Common/Library/CommonLibrary';

export default function WorkOrderOperationDetailsOnReturning(context) {
	let caption = toolbarCaption(context);
	context.setToolbarItemCaption('IssuePartTbI', context.localizeText(caption));

	//CC Refresh View to force update of toolbar buttons
	CommonLibrary.refreshPage(context);

	let pageToolbar;

	let page = context.evaluateTargetPath('#Page:WorkOrderOperationDetailsPage');
	if (page) {
		pageToolbar = page.getToolbar();
	}

	if (pageToolbar) {
		pageToolbar.then(function (toolbar) {
			var toolbarItems = toolbar.getToolbarItems();
			//set visability of scanButton & Confirm Operation Button
			if (toolbarItems[1].name === "ScanAssetBtn") {
				toolbarItems[1].setVisibility("hidden");
				toolbarItems[2].setVisibility("visible");
			}

		});
	}
}