export default function MaterialLstPkrQueryOptions(context) {
    let plant = context.getPageProxy().binding.Plant;
    let storageLoc = context.getPageProxy().binding.StorageLocation;
    if (plant && storageLoc) {
        return `$orderby=MaterialNum&$expand=Material,MaterialPlant&$filter=Plant eq '${plant}' and StorageLocation eq '${storageLoc}'`;
    } else if (plant) {
        return `$orderby=MaterialNum&$expand=Material,MaterialPlant&$filter=Plant eq '${plant}'`;
    } else {
        return '$orderby=MaterialNum&$expand=Material,MaterialPlant';
    }
}
