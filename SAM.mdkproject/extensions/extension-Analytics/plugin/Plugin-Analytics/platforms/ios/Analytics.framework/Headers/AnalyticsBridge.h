//
//  AnalyticsBridge.h
//  Analytics
//
//  Created by Kieselmann, Markus on 07/03/2017.
//  Copyright © 2017 SAP SE. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "AnalyticsDelegate.h"

/**
 Callback to be called from SEAM plugin.

 @param data data returned in callback
 @param type Corresponding request type
 */
typedef void (^AnalyticsBridgeCallback)(NSDictionary* data, NSString* type);

@interface AnalyticsBridge : NSObject
@property (nonatomic, copy) AnalyticsBridgeCallback callback;
-(UIView*) createWithParams:(NSDictionary*)params andDelegate:(AnalyticsDelegate*)delegate;

@end
