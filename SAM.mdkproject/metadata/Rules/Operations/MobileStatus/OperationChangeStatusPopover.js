import libOprMobile from './OperationMobileStatusLibrary';

export default function OperationChangeStausPopover(context) {
    
    return libOprMobile.operationStatusPopoverMenu(context);
}
