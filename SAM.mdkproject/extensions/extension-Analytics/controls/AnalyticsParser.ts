import { BaseObservable } from '../../../observables/BaseObservable';
import { Context } from '../../../context/Context';
import { IControl } from '../../../controls/IControl';
import { BaseExtensionParser } from './BaseExtensionParser';
import { Utils } from './Utils';
import { EventHandler } from '../../../EventHandler';

export class AnalyticsParser extends BaseExtensionParser {

    public parse(fromValue: any, context: Context, key: string): Promise<any> {
        if (fromValue) {
            let formatRule = this.getFormatRule(fromValue);
            fromValue = fromValue.replace(this._formatPattern, '}');
            let toValue = super.parseValue(fromValue, context);
            if (formatRule) {
                return this.applyFormat(formatRule, toValue, context, key);
            }
            return this.createPromiseValue(key, toValue);
         }

        return this.createPromiseValue(key, fromValue);
    }
};
