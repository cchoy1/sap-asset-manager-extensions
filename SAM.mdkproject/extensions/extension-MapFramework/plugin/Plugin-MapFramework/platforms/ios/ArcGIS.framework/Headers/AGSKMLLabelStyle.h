/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

/** @file AGSKMLLabelStyle.h */ //Required for Globals API doc

#import <ArcGIS/AGSKMLColorStyle.h>

NS_ASSUME_NONNULL_BEGIN

/** @brief Specifies how the name of a @c AGSKMLNode is drawn, including color and scale.

 Controls how the @c AGSKMLNode#name is displayed. Corresponds to a <LabelStyle> in a KML
 document. Controls the color and scale of the name label.
 @see @c AGSKMLNode#name
 @since 100.6
 */
@interface AGSKMLLabelStyle : AGSKMLColorStyle

#pragma mark -
#pragma mark initializers

-(instancetype)init NS_UNAVAILABLE;

/** Creates a KML label style with a specified label color and scale.
 @param color Color of the label.
 @param scale Scale of the label.
 @since 100.6
 */
-(instancetype)initWithColor:(AGSColor *)color 
                       scale:(double)scale;

/** Creates a KML label style with a specified label color and scale.
 @param color Color of the label.
 @param scale Scale of the label.
 @since 100.6
 */
+(instancetype)KMLLabelStyleWithColor:(AGSColor *)color 
                                scale:(double)scale;

#pragma mark -
#pragma mark properties

/** Scale of the label.
 @since 100.6
 */
@property (nonatomic, assign, readwrite) double scale;

#pragma mark -
#pragma mark methods

@end

NS_ASSUME_NONNULL_END
