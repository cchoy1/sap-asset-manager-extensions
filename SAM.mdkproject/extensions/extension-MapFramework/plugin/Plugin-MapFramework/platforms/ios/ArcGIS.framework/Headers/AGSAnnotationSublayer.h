/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

/** @file AGSAnnotationSublayer.h */ //Required for Globals API doc

#import <ArcGIS/AGSObject.h>
#import <ArcGIS/AGSLayerContent.h>

NS_ASSUME_NONNULL_BEGIN

/** @brief An instance of this class allows you to interrogate the properties of a Sublayer within an Annotation layer, and to change the visibility of the sublayer
 
 You can get a collection of sublayers (@c AGSAnnotationSublayer) from the annotation layer using
 @c AGSLayerContent#subLayerContents.
 @c AGSAnnotationSublayer objects only exist as part of an @c AGSAnnotationLayer object.
 They cannot exist separately.
 They are populated when the annotation layer is loaded.
 @since 100.6
 */
@interface AGSAnnotationSublayer : AGSObject <AGSLayerContent>

#pragma mark -
#pragma mark initializers

-(instancetype)init NS_UNAVAILABLE;

#pragma mark -
#pragma mark properties

/** The where-clause of the sql expression that defines which annotation features from the annotation layer's feature table are included in this Sublayer.
 @since 100.6
 */
@property (nonatomic, copy, readonly) NSString *definitionExpression;

/** The maximum scale at which to display the Sublayer.
 @since 100.6
 */
@property (nonatomic, assign, readonly) double maxScale;

/** The minimum scale at which to display the Sublayer.
 Zero indicates that the annotation will be visible no matter how far the user zooms out.
 @since 100.6
 */
@property (nonatomic, assign, readonly) double minScale;

/** The opacity with which to display the annotation text associated with this Sublayer.
 Opacity is a value between 0 and 1, with 0 indicating that the annotation will be completely transparent, and 1 indicating that the annotation will be completely opaque.
 @since 100.6
 */
@property (nonatomic, assign, readonly) float opacity;

/** The property specifies whether the annotation text scales with the @c AGSMapView.
 For annotation layers and sublayers this is always true.
 @since 100.6
 */
@property (nonatomic, assign, readonly) BOOL scaleSymbols;

/** Non-negative integer id number of the sublayer within its annotation layer
 @since 100.6
 */
@property (nonatomic, assign, readonly) NSInteger sublayerID;

#pragma mark -
#pragma mark methods

@end

NS_ASSUME_NONNULL_END
